package entities;

import java.util.List;

public class Movie {

    private long id;
    private String name;
    private List<String> genres;

    public Movie(int id, String name, List<String> genres) {
        this.id = id;
        this.name = name;
        this.genres = genres;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<String> getGenres() {
        return genres;
    }

    public void setGenres(List<String> genres) {
        this.genres = genres;
    }
}
