import entities.Genre;
import org.apache.mahout.cf.taste.common.TasteException;
import org.apache.mahout.cf.taste.impl.common.LongPrimitiveIterator;
import org.apache.mahout.cf.taste.impl.model.file.FileDataModel;
import org.apache.mahout.cf.taste.impl.similarity.TanimotoCoefficientSimilarity;
import org.apache.mahout.cf.taste.model.DataModel;
import org.apache.mahout.cf.taste.model.PreferenceArray;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

public class Main {

    //region Attributes
    private static DataModel userDataModel;
    private static Map<Long, Genre> genreMap = new HashMap<>();
    //endregion

    //region Main
    public static void main(String... args) throws IOException, TasteException {

        String genre1 = args.length > 0 ? args[0] : "Action";
        String genre2 = args.length > 1 ? args[1] : "Horror";

        fillGenreMap();

        long genre1Id = genreMap.keySet().stream().filter(k -> genreMap.get(k).getName().equalsIgnoreCase(genre1)).findFirst().get();
        long genre2Id = genreMap.keySet().stream().filter(k -> genreMap.get(k).getName().equalsIgnoreCase(genre2)).findFirst().get();

        userDataModel = getUserDataModel();

        userDataModel.getPreferencesForItem(genre1Id);

        TanimotoCoefficientSimilarity tanimotoCoefficientSimilarity = new TanimotoCoefficientSimilarity(userDataModel);
        tanimotoCoefficientSimilarity.itemSimilarities(genre1Id, new long[]{genre2Id});

        double matchPercentage = tanimotoCoefficientSimilarity.itemSimilarities(genre1Id, new long[]{genre2Id})[0];

        System.out.println("Al " + Math.round(matchPercentage * 100) + "% de las personas que les gustan las películas de " + genre1 + " le gustan las de " + genre2);

        assert matchPercentage >= 0.6;


    }

    //endregion

    //region Private Methods
    private static FileDataModel getUserDataModel() throws IOException {
        File file = new File(Main.class.getResource("ratings_genre.csv").getFile());
        return new FileDataModel(file);
    }

    private static void fillGenreMap() throws IOException {
        File file = new File(Main.class.getResource("genres.csv").getFile());
        BufferedReader br = new BufferedReader(new FileReader(file));
        String line;
        while ((line = br.readLine()) != null) {
            String[] parts = line.split(",");
            Genre genre = new Genre(Integer.valueOf(parts[0]), parts[1]);
            genreMap.put(genre.getId(), genre);
        }
    }
    //endregion
}
